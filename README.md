# Eagle's Server

Eagle's Creative server is Minecraft server, that uses PaperMC plugins.
There are list of almost all these plugins.

## Kernel

Eagle's Creative uses original PaperMC software for running server. We update our server kernel on its new stable release with our plugins.

PaperMC can be downloaded from it's [https://papermc.io/](website).

Currently we're using version ``1.21.4``.

## Plugins

Eagle's Creative uses plugins made for Spigot or Paper servers. *Some plugins are hidden from this list.* Download plugins from Spigot or Paper sites. List of plugins:

### [OpenCreative+](https://modrinth.com/plugin/opencreative)
This plugin allows players to create their worlds. It's developed by McChicken Studio and Eagle's Creative.

### [AdvancedBan](https://www.spigotmc.org/resources/advancedban.8695/)
We use this plugin for player's punishments. You can mute, kick, ban, warn or even see history of player's punishments. Also you can translate messages.

### [ArmorStandEditor](https://www.spigotmc.org/resources/armorstandeditor-reborn.94503/)
With this plugin players can create models from armor stands. They can change pose of armor stands; arm, leg, head, torso positions; size; gravity.

### [AuthMe](https://www.spigotmc.org/resources/authmereloaded.6269/)
This is an old plugin with long history. Almost every server uses it for player's registration and login.

### [Skulls](https://www.spigotmc.org/resources/skulls-the-ultimate-head-database.90098/)
With this plugin players can choose thousands heads from one big database. Decorate your world!

### [Builders Utilities](https://modrinth.com/plugin/buildersutilities)
This plugin gives builders useful tools like NoClip, Slab Changer, Speed Changer, Colored Armor Picker.

### [FastAsyncWorldEdit](https://www.spigotmc.org/resources/fastasyncworldedit.13932/)
Without this plugin any builder will be tired, because it simplifies building in many times. Create boxes, walls, spheres and more.

### [Essentials](https://www.spigotmc.org/resources/essentialsx.9089/)
Because of this plugin we have common commands like /tp, /heal, /ec, /mail, /balance, /jail, /gm, /warp and more.


### [GSit](https://www.spigotmc.org/resources/gsit-modern-sit-seat-and-chair-lay-and-crawl-plugin-1-16-1-20-6.62325/)
This plugin extends roleplay servers. With it players can sit on blocks, lay or even crawl.

### [Jukebox](https://www.spigotmc.org/resources/jukebox-music-plugin.40580/)
Import .NBS files and listen beautiful note block songs.


### [NameTagEdit](https://www.spigotmc.org/resources/nametagedit.3836/)
This plugin is responsible for prefixes and suffixes in tablist and over player's head.


### [Citizens](https://www.spigotmc.org/resources/citizens.13811/)
We create NPC's on spawn with this plugin, so you can interact with them.


### [LuckPerms](https://www.spigotmc.org/resources/luckperms.28140/)
Every server needs to control player's permissions. Change permissions for world's creation!


### [Vault](https://www.spigotmc.org/resources/vault.34315/) 
Storage for variables, permissions, economy.


### [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/) 
Replaces placeholders with some information.
